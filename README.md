## kthash, a Kotlin hashing library

kthash implements the [FIPS 180-4] hashing algorithms, as well as the
historical MD4 and MD5 algorithms, within [Guava]'s `HashFunction`
framework. This is a significantly simpler interface to implement than
Java's `MessageDigestSpi` interface, plus it makes it (theoretically)
compatible with Kotlin/JS when used with guava-gwt (though that's not
a tested configuration yet, and the code currently uses JVM-specific
interfaces in a few places).

### Usage

The singleton hash functions—`SHA1`, `SHA224`, `SHA256`, `SHA384`,
`SHA512`, `MD4`, and `MD5`—can all be used like this:

```kotlin
val hasher = SHA256.newHasher()
hasher.putString("Hello, world!", Charsets.UTF_8)
println(hasher.hash())
// 315f5bdb76d078c43b8ac0064e4a0164612b1fce77c869345bfc94c75894edd3
```

The SHA-512/t hash functions can be used like this:

```kotlin
val SHA512_256 = SHA512t(256)
val hasher = SHA512_256.newHasher()
hasher.putString("Hello, world!", Charsets.UTF_8)
println(hasher.hash())
// 330c723f25267587db0b9f493463e017011239169cb57a6db216c63774367115
```

Once `hash()` is called on a `Hasher` instance, no further data can be
added to it; attempting to do so will cause `IllegalStateException` to
be thrown. `hash()` can be called multiple times on the same `Hasher`
instance, and will return equal values each time.

[FIPS 180-4]: https://csrc.nist.gov/publications/detail/fips/180/4/final
[Guava]: https://github.com/google/guava
