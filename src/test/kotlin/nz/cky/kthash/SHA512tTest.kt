package nz.cky.kthash

import kotlin.test.Test

class SHA512tTest {
    @Test
    fun standardTests() {
        assertHash224("4634270f707b6a54daae7530460842e20e37ed265ceee9a43e8924aa", "abc")
        assertHash224(
            "23fec5bb94d60b23308192640b0c453335d664734fe40e7268674af9",
            "abcdefghbcdefghicdefghijdefghijkefghijklfghijklmghijklmnhijklmnoijklmnopjklmnopqklmnopqrlmnopqrsmnopqrstnopqrstu"
        )
        assertHash256("53048e2681941ef99b2e29b76b4c7dabe4c2d0c634fc6d46e0e2f13107e7af23", "abc")
        assertHash256(
            "3928e184fb8690f840da3988121d31be65cb9d3ef83ee6146feac861e19b563a",
            "abcdefghbcdefghicdefghijdefghijkefghijklfghijklmghijklmnhijklmnoijklmnopjklmnopqklmnopqrlmnopqrsmnopqrstnopqrstu"
        )
    }

    private companion object {
        private fun assertHash224(expected: String, input: String) =
            TextValidator(SHA512t(224)).assertHash(expected, input)

        private fun assertHash256(expected: String, input: String) =
            TextValidator(SHA512t(256)).assertHash(expected, input)
    }
}
