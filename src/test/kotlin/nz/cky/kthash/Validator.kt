@file:Suppress("UnstableApiUsage")

package nz.cky.kthash

import com.google.common.hash.HashCode
import com.google.common.hash.HashFunction
import com.google.common.hash.Hasher
import com.google.common.io.ByteStreams
import java.nio.channels.WritableByteChannel
import kotlin.test.assertEquals

abstract class Validator(protected val hashFunction: HashFunction) {
    fun assertHash(expected: String, hash: HashCode) =
        assertEquals(HashCode.fromString(expected), hash)

    fun assertHash(expected: String, value: UByte, count: Int) =
        assertHash(expected) {
            ByteStreams.copy(
                ConstantByteChannel(value, count),
                this as WritableByteChannel
            )
        }

    fun assertHash(expected: String, block: Hasher.() -> Unit) =
        assertHash(expected, hashFunction.hash(block))

    abstract fun assertHash(expected: String, input: String)
}
