package nz.cky.kthash

import kotlin.test.Test

class SHA1Test {
    @Test
    fun standardTests() {
        assertHash("a9993e364706816aba3e25717850c26c9cd0d89d", "abc")
        assertHash(
            "84983e441c3bd26ebaae4aa1f95129e5e54670f1",
            "abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq"
        )
    }

    private companion object : TextValidator(SHA1)
}
