package nz.cky.kthash

import kotlin.test.Test

class SHA224Test {
    @Test
    fun standardTests() {
        assertHash("23097d223405d8228642a477bda255b32aadbce4bda0b3f7e36c9da7", "abc")
        assertHash(
            "75388b16512776cc5dba5da1fd890150b0c6455cb4f58b1952522525",
            "abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq"
        )
    }

    @Test
    fun extraTests() {
        assertHash("e33f9d75e6ae1369dbabf81b96b4591ae46bba30b591a6b6c62542b5", 255U, 1)
        assertHash("fd19e74690d291467ce59f077df311638f1c3a46e510d0e49a67062d") { putInt(0x2499e0e5) }
        assertHash("5c3e25b69d0ea26f260cfae87e23759e1eca9d1ecc9fbf3c62266804", 0U, 56)
        assertHash("3706197f66890a41779dc8791670522e136fafa24874685715bd0a8a", 0x51U, 1000)
        assertHash("a8d0c66b5c6fdfd836eb3c6d04d32dfe66c3b1f168b488bf4c9c66ce", 0x41U, 1000)
        assertHash("cb00ecd03788bf6c0908401e0eb053ac61f35e7e20a2cfd7bd96d640", 0x99U, 1005)
        assertHash("3a5d74b68f14f3a4b2be9289b8d370672d0b3d2f53bc303c59032df3", 0U, 1000000)
    }

    @Test
    fun lengthyTests() {
        assertHash("c4250083cf8230bf21065b3014baaaf9f76fecefc21f91cf237dedc9", 0x41U, 0x20000000)
        assertHash("014674abc5cb980199935695af22fab683748f4261d4c6492b77c543", 0U, 0x41000000)
        assertHash("a654b50b767a8323c5b519f467d8669837142881dc7ad368a7d5ef8f", 0x84U, 0x6000003f)
    }

    private companion object : TextValidator(SHA224)
}
