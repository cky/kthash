@file:Suppress("UnstableApiUsage")

package nz.cky.kthash

import com.google.common.hash.HashFunction

open class TextValidator(hashFunction: HashFunction) : Validator(hashFunction) {
    override fun assertHash(expected: String, input: String) =
        assertHash(expected, hashFunction.hashString(input, Charsets.US_ASCII))
}
