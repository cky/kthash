package nz.cky.kthash

import kotlin.test.Test

class SHA256Test {
    @Test
    fun standardTests() {
        assertHash("ba7816bf8f01cfea414140de5dae2223b00361a396177a9cb410ff61f20015ad", "abc")
        assertHash(
            "248d6a61d20638b8e5c026930c3e6039a33ce45964ff2167f6ecedd419db06c1",
            "abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq"
        )
    }

    @Test
    fun extraTests() {
        assertHash("68325720aabd7c82f30f554b313d0570c95accbb7dc4b5aae11204c08ffe732b", 0xbdU, 1)
        assertHash("7abc22c0ae5af26ce93dbb94433a0e0b2e119d014f8e7f65bd56c61ccccd9504") { putInt(0x558e8cc9) }
        assertHash("02779466cdec163811d078815c633f21901413081449002f24aa3e80f0b88ef7", 0U, 55)
        assertHash("d4817aa5497628e7c77e6b606107042bbba3130888c5f47a375e6179be789fbb", 0U, 56)
        assertHash("541b3e9daa09b20bf85fa273e5cbd3e80185aa4ec298e765db87742b70138a53", 0U, 1000)
        assertHash("c2e686823489ced2017f6059b8b239318b6364f6dcd835d0a519105a1eadd6e4", 0x41U, 1000)
        assertHash("f4d62ddec0f3dd90ea1380fa16a5ff8dc4c54b21740650f24afc4120903552b0", 0x55U, 1005)
        assertHash("d29751f2649b32ff572b5e0a9f541ea660a50f94ff0beedfb0b692b924cc8025", 0U, 1000000)
    }

    @Test
    fun lengthyTests() {
        assertHash("15a1868c12cc53951e182344277447cd0979536badcc512ad24c67e9b2d4f3dd", 0x5aU, 0x20000000)
        assertHash("461c19a93bd4344f9215f5ec64357090342bc66b15a148317d276e31cbc20b53", 0U, 0x41000000)
        assertHash("c23ce8a7895f4b21ec0daf37920ac0a262a220045a03eb2dfed48ef9b05aabea", 0x42U, 0x6000003e)
    }

    private companion object : TextValidator(SHA256)
}
