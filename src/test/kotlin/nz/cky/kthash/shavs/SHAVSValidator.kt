@file:Suppress("UnstableApiUsage")

package nz.cky.kthash.shavs

import com.google.common.hash.HashCode
import com.google.common.hash.HashFunction
import com.google.common.io.BaseEncoding
import com.google.common.io.ByteStreams
import nz.cky.kthash.Validator
import nz.cky.kthash.hash
import java.io.StringReader
import java.nio.channels.Channels
import java.nio.channels.WritableByteChannel

open class SHAVSValidator(hashFunction: HashFunction) : Validator(hashFunction) {
    override fun assertHash(expected: String, input: String) =
        assertHash(expected) {
            ByteStreams.copy(
                Channels.newChannel(LOWER_CASE_HEX.decodingStream(StringReader(input))),
                this as WritableByteChannel
            )
        }

    fun assertMonteCarlo(seed: String, vararg checkpoints: String) {
        checkpoints.fold(LOWER_CASE_HEX.decode(seed)) { prev, cp ->
            val hash = (1..1000).fold(Triple(prev, prev, prev)) { (a, b, c), _ ->
                Triple(b, c, hashFunction.putBytes(a, b, c).asBytes())
            }.third
            assertHash(cp, HashCode.fromBytes(hash))
            hash
        }
    }

    private companion object {
        private val LOWER_CASE_HEX: BaseEncoding = BaseEncoding.base16().lowerCase()

        private fun HashFunction.putBytes(vararg arrays: ByteArray): HashCode =
            hash { arrays.forEach { putBytes(it) } }
    }
}
