package nz.cky.kthash

import java.nio.ByteBuffer
import java.nio.channels.ReadableByteChannel
import kotlin.math.max
import kotlin.math.min

class ConstantByteChannel(private val value: UByte, private var count: Int) : ReadableByteChannel {
    private var cache = UByteArray(0)

    override fun isOpen(): Boolean = true
    override fun close() = Unit

    override fun read(dst: ByteBuffer): Int = when (val left = count) {
        0 -> -1
        else -> min(left, dst.remaining()).also { toRead ->
            if (cache.size < toRead) {
                val newSize = max(2 * cache.size, toRead)
                cache = UByteArray(newSize) { value }
            }
            dst.put(cache.asByteArray(), 0, toRead)
            count -= toRead
        }
    }
}
