@file:Suppress("UnstableApiUsage")

package nz.cky.kthash

import com.google.common.hash.HashCode
import com.google.common.hash.Hasher
import nz.cky.kthash.impl.DefaultedHashFunction
import nz.cky.kthash.impl.IntMerkleDamgård
import nz.cky.kthash.impl.SHA224_INIT
import nz.cky.kthash.impl.SHA256Compressor
import java.nio.ByteBuffer

object SHA224 : DefaultedHashFunction {
    override fun newHasher(): Hasher = SHA224Hasher()

    override fun bits(): Int = 224

    private class SHA224Hasher : IntMerkleDamgård(SHA256Compressor, SHA224_INIT, 16) {
        override fun hash(): HashCode {
            val buf = ByteBuffer.allocate(28)
            buf.asIntBuffer().put(hashAsUIntArray().asIntArray(), 0, 7)
            return HashCode.fromBytes(buf.array())
        }
    }
}
