@file:Suppress("UnstableApiUsage")

package nz.cky.kthash

import com.google.common.hash.Hasher
import nz.cky.kthash.impl.DefaultedHashFunction
import nz.cky.kthash.impl.IntMerkleDamgård
import nz.cky.kthash.impl.MD4Compressor
import nz.cky.kthash.impl.MD5_INIT
import java.nio.ByteOrder

object MD4 : DefaultedHashFunction {
    override fun newHasher(): Hasher = MD4Hasher()

    override fun bits(): Int = 128

    private class MD4Hasher : IntMerkleDamgård(
        MD4Compressor, MD5_INIT, 16, ByteOrder.LITTLE_ENDIAN
    )
}
