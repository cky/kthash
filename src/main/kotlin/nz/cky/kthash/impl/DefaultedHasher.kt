/*
 * Copyright (C) 2011 The Guava Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you
 * may not use this file except in compliance with the License. You may
 * obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

@file:Suppress("UnstableApiUsage")

package nz.cky.kthash.impl

import com.google.common.base.Preconditions
import com.google.common.hash.Funnel
import com.google.common.hash.Hasher
import java.nio.ByteBuffer
import java.nio.charset.Charset

/**
 * Default implementations of [Hasher] methods copied over from
 * Guava's `AbstractHasher`, but as default methods.
 */
internal interface DefaultedHasher : Hasher {
    @JvmDefault
    override fun putBoolean(b: Boolean): Hasher =
        putByte(if (b) 1 else 0)

    @JvmDefault
    override fun putDouble(d: Double): Hasher =
        putLong(java.lang.Double.doubleToRawLongBits(d))

    @JvmDefault
    override fun putFloat(f: Float): Hasher =
        putInt(java.lang.Float.floatToRawIntBits(f))

    @JvmDefault
    override fun putUnencodedChars(charSequence: CharSequence): Hasher =
        charSequence.fold(this, Hasher::putChar)

    @JvmDefault
    override fun putString(charSequence: CharSequence, charset: Charset): Hasher =
        putBytes(charSequence.toString().toByteArray(charset))

    @JvmDefault
    override fun putBytes(bytes: ByteArray): Hasher =
        putBytes(bytes, 0, bytes.size)

    @JvmDefault
    override fun putBytes(bytes: ByteArray, off: Int, len: Int): Hasher = apply {
        Preconditions.checkPositionIndexes(off, off + len, bytes.size)
        (off until off + len).forEach { putByte(bytes[it]) }
    }

    @JvmDefault
    override fun putBytes(b: ByteBuffer): Hasher = apply {
        if (b.hasArray()) {
            putBytes(b.array(), b.arrayOffset() + b.position(), b.remaining())
            b.position(b.limit())
        } else {
            repeat(b.remaining()) { putByte(b.get()) }
        }
    }

    @JvmDefault
    override fun putShort(s: Short): Hasher = putByte(s.toByte())
        .putByte(s.toInt().ushr(8).toByte())

    @JvmDefault
    override fun putInt(i: Int): Hasher = putByte(i.toByte())
        .putByte(i.ushr(8).toByte())
        .putByte(i.ushr(16).toByte())
        .putByte(i.ushr(24).toByte())

    @JvmDefault
    override fun putLong(l: Long): Hasher = apply {
        (0 until 64 step 8).forEach { putByte(l.ushr(it).toByte()) }
    }

    @JvmDefault
    override fun putChar(c: Char): Hasher = putByte(c.toByte())
        .putByte(c.toInt().ushr(8).toByte())

    @JvmDefault
    override fun <T : Any> putObject(instance: T, funnel: Funnel<in T>): Hasher =
        apply { funnel.funnel(instance, this) }
}
