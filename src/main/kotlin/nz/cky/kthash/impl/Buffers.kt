package nz.cky.kthash.impl

import java.nio.Buffer

internal var Buffer.position: Int
    get() = position()
    set(value) {
        position(value)
    }

internal var Buffer.limit: Int
    get() = limit()
    set(value) {
        limit(value)
    }
