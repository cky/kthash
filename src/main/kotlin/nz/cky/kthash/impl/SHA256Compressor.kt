package nz.cky.kthash.impl

internal object SHA256Compressor : Compressor<UIntArray> {
    override fun compress(hash: UIntArray, message: UIntArray): UIntArray {
        require(hash.size == 8)
        val schedule = rollingSequence(message) {
            a + b.σ0 + j + o.σ1
        }
        return SHA256_CONST.asSequence().zip(schedule) { a, b -> a + b }
            .fold(hash) { (a, b, c, d, e, f, g, h), i ->
                val t1 = h + e.Σ1 + choose(e, f, g) + i
                uintArrayOf(
                    t1 + a.Σ0 + majority(a, b, c),
                    a, b, c, d + t1, e, f, g
                )
            }
    }
}
