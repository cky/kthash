package nz.cky.kthash.impl

internal object SHA1Compressor : Compressor<UIntArray> {
    override fun compress(hash: UIntArray, message: UIntArray): UIntArray {
        require(hash.size == 5)
        val schedule = rollingSequence(message) {
            (a xor c xor i xor n) rotateLeft 1
        }
        return schedule.take(80).foldIndexed(hash) { round, (a, b, c, d, e), i ->
            uintArrayOf(
                (a rotateLeft 5) + roundSpecific(round, b, c, d) + e + i,
                a, b rotateLeft 30, c, d
            )
        }
    }

    private fun roundSpecific(round: Int, b: UInt, c: UInt, d: UInt): UInt = when (round) {
        in 0..19 -> choose(b, c, d) + SHA1_CONST[0]
        in 20..39 -> parity(b, c, d) + SHA1_CONST[1]
        in 40..59 -> majority(b, c, d) + SHA1_CONST[2]
        in 60..79 -> parity(b, c, d) + SHA1_CONST[3]
        else -> throw IllegalArgumentException("Invalid round: $round")
    }
}
