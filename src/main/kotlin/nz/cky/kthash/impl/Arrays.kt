package nz.cky.kthash.impl

internal operator fun UIntArray.component6(): UInt = get(5)
internal operator fun UIntArray.component7(): UInt = get(6)
internal operator fun UIntArray.component8(): UInt = get(7)

internal operator fun ULongArray.component6(): ULong = get(5)
internal operator fun ULongArray.component7(): ULong = get(6)
internal operator fun ULongArray.component8(): ULong = get(7)
