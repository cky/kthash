package nz.cky.kthash.impl

internal interface Compressor<T> {
    fun compress(hash: T, message: T): T
}
