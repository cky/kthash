@file:Suppress("NonAsciiCharacters", "FunctionName", "ObjectPropertyName")

package nz.cky.kthash.impl

internal infix fun UInt.rotateLeft(count: Int): UInt = Integer.rotateLeft(toInt(), count).toUInt()
internal infix fun ULong.rotateLeft(count: Int): ULong = java.lang.Long.rotateLeft(toLong(), count).toULong()

internal infix fun UInt.rotateRight(count: Int): UInt = Integer.rotateRight(toInt(), count).toUInt()
internal infix fun ULong.rotateRight(count: Int): ULong = java.lang.Long.rotateRight(toLong(), count).toULong()

internal fun choose(x: UInt, y: UInt, z: UInt): UInt = (x and y) xor (x.inv() and z)
internal fun choose(x: ULong, y: ULong, z: ULong): ULong = (x and y) xor (x.inv() and z)

internal fun majority(x: UInt, y: UInt, z: UInt): UInt = parity(x and y, x and z, y and z)
internal fun majority(x: ULong, y: ULong, z: ULong): ULong = parity(x and y, x and z, y and z)

internal fun parity(x: UInt, y: UInt, z: UInt): UInt = x xor y xor z
internal fun parity(x: ULong, y: ULong, z: ULong): ULong = x xor y xor z

private fun UInt.Σ(a: Int, b: Int, c: Int): UInt = rotateRight(a) xor rotateRight(b) xor rotateRight(c)
private fun ULong.Σ(a: Int, b: Int, c: Int): ULong = rotateRight(a) xor rotateRight(b) xor rotateRight(c)
private fun UInt.σ(a: Int, b: Int, c: Int): UInt = rotateRight(a) xor rotateRight(b) xor shr(c)
private fun ULong.σ(a: Int, b: Int, c: Int): ULong = rotateRight(a) xor rotateRight(b) xor shr(c)

internal val UInt.Σ0: UInt get() = Σ(2, 13, 22)
internal val ULong.Σ0: ULong get() = Σ(28, 34, 39)

internal val UInt.Σ1: UInt get() = Σ(6, 11, 25)
internal val ULong.Σ1: ULong get() = Σ(14, 18, 41)

internal val UInt.σ0: UInt get() = σ(7, 18, 3)
internal val ULong.σ0: ULong get() = σ(1, 8, 7)

internal val UInt.σ1: UInt get() = σ(17, 19, 10)
internal val ULong.σ1: ULong get() = σ(19, 61, 6)
