@file:Suppress("NonAsciiCharacters", "ClassName", "UnstableApiUsage")

package nz.cky.kthash.impl

import com.google.common.hash.HashCode
import com.google.common.hash.Hasher
import java.nio.ByteBuffer
import java.nio.channels.WritableByteChannel

internal open class LongMerkleDamgård(
    private val compressor: Compressor<ULongArray>,
    initialHash: List<ULong>,
    private val messageSize: Int
) : DefaultedHasher, WritableByteChannel {
    private val hash: ULongArray = initialHash.toULongArray()
    private val buffer: ByteBuffer = ByteBuffer.allocate(messageSize * WORD_SIZE)
    private var countHigh: ULong = 0U
    private var countLow: ULong = 0U
    private var done: Boolean = false

    override fun putByte(b: Byte): Hasher = apply {
        checkPutPreconditions()
        buffer.put(b)
        compressIfNecessary()
        addToCounter(1)
    }

    override fun putBytes(b: ByteBuffer): Hasher = apply { write(b) }
    override fun putBytes(bytes: ByteArray): Hasher = putBytes(ByteBuffer.wrap(bytes))
    override fun putBytes(bytes: ByteArray, off: Int, len: Int): Hasher =
        putBytes(ByteBuffer.wrap(bytes, off, len))

    override fun hash(): HashCode {
        if (!done) {
            putPadding()
        }
        val buf = ByteBuffer.allocate(hash.size * WORD_SIZE)
        buf.asLongBuffer().put(hash.asLongArray())
        return HashCode.fromBytes(buf.array())
    }

    private fun checkPutPreconditions() {
        assert(buffer.hasRemaining())
        check(!done) { "Can't add more data after hash() has been called" }
    }

    internal fun hashAsULongArray(): ULongArray {
        if (!done) {
            putPadding()
        }
        return hash
    }

    private fun putPadding() {
        checkPutPreconditions()
        buffer.put(-128)
        compressIfNecessary()
        if (buffer.remaining() < COUNTER_SIZE) {
            buffer.put(ByteArray(buffer.remaining()))
            compress()
            assert(buffer.remaining() >= COUNTER_SIZE)
        }
        if (buffer.remaining() > COUNTER_SIZE) {
            buffer.put(ByteArray(buffer.remaining() - COUNTER_SIZE))
        }
        buffer.putLong(countHigh.toLong())
        buffer.putLong(countLow.toLong())
        compress()
        done = true
    }

    private fun compress() {
        assert(!buffer.hasRemaining())
        val message = ULongArray(messageSize)
        buffer.flip().asLongBuffer().apply {
            get(message.asLongArray())
            buffer.position += position * WORD_SIZE
        }
        val result = compressor.compress(hash, message)
        assert(result.size == hash.size)
        result.forEachIndexed { i, value ->
            hash[i] += value
        }
        buffer.compact()
    }

    private fun compressIfNecessary() {
        if (!buffer.hasRemaining()) {
            compress()
        }
    }

    private fun addToCounter(size: Int) {
        assert(size >= 0)
        val newLow = countLow + size.toULong() * Byte.SIZE_BITS.toUInt()
        if (newLow < countLow) {
            check(countHigh != ULong.MAX_VALUE) { "Too much data to hash" }
            ++countHigh
        }
        countLow = newLow
    }

    override fun isOpen(): Boolean = !done

    override fun close() {
        done = true
    }

    override fun write(src: ByteBuffer): Int {
        checkPutPreconditions()
        var total = 0
        while (src.remaining() > buffer.remaining()) {
            val slice = src.slice()
            val size = buffer.remaining()
            slice.limit = size
            buffer.put(slice)
            compress()
            src.position += size
            addToCounter(size)
            total += size
        }
        val size = src.remaining()
        buffer.put(src)
        compressIfNecessary()
        addToCounter(size)
        return total + size
    }

    private companion object {
        private const val WORD_SIZE: Int = ULong.SIZE_BYTES
        private const val COUNTER_SIZE: Int = 2 * ULong.SIZE_BYTES
    }
}
