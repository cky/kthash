package nz.cky.kthash.impl

internal object MD4Compressor : Compressor<UIntArray> {
    override fun compress(hash: UIntArray, message: UIntArray): UIntArray {
        require(hash.size == 4)
        require(message.size == 16)
        return (0..47).fold(hash) { (a, b, c, d), round ->
            uintArrayOf(
                d,
                (a + roundSpecific(round, b, c, d) + message[indexForRound(round)])
                        rotateLeft rotationForRound(round),
                b, c
            )
        }
    }

    private fun invalidRound(round: Int): Nothing =
        throw IllegalArgumentException("Invalid round: $round")

    private fun roundSpecific(round: Int, b: UInt, c: UInt, d: UInt): UInt = when (round) {
        in 0..15 -> choose(b, c, d)
        in 16..31 -> majority(b, c, d) + SHA1_CONST[0]
        in 32..47 -> parity(b, c, d) + SHA1_CONST[1]
        else -> invalidRound(round)
    }

    private fun indexForRound(round: Int): Int = when (round) {
        in 0..15 -> round
        in 16..31 -> 4 * (round % 4) + (round - 16) / 4
        in 32..47 -> java.lang.Integer.reverse(round).ushr(28)
        else -> invalidRound(round)
    }

    private fun rotationForRound(round: Int): Int = (round % 4).let {
        it * 4 + 3 +
                when (round) {
                    in 0..15 -> if (it == 3) 4 else 0
                    in 16..31 -> if (it != 0) -2 else 0
                    in 32..47 -> if (it == 1) 2 else 0
                    else -> invalidRound(round)
                }
    }
}
