package nz.cky.kthash.impl

import java.math.BigDecimal
import java.math.BigInteger
import java.math.MathContext
import java.math.RoundingMode
import kotlin.math.absoluteValue
import kotlin.math.sin
import kotlin.math.sqrt

private val PRIMES: Sequence<Int> = Sequence { PrimesUpTo(409) }

private val MC: MathContext = MathContext(23, RoundingMode.HALF_EVEN)
private val HALF: BigDecimal = BigDecimal.valueOf(5, 1)
private val THIRD: BigDecimal = BigDecimal.ONE.divide(3.toBigDecimal(), MC)
private val TWO_TO_64: BigDecimal = BigInteger.ZERO.setBit(64).toBigDecimal()

private fun sqrtBD(x: Int): BigDecimal = sqrt(x.toDouble()).toBigDecimal().let { y ->
    (x.toBigDecimal().divide(y, MC) + y) * HALF
}

private fun cbrtBD(x: Int): BigDecimal = Math.cbrt(x.toDouble()).toBigDecimal().let { y ->
    (x.toBigDecimal().divide(y.multiply(y), MC) + y + y) * THIRD
}

private fun Double.frac32(): UInt = Math.scalb(this, 32).toLong().toUInt()
private fun BigDecimal.frac64(): ULong = multiply(TWO_TO_64).toLong().toULong()
private fun UInt.reverseBytes(): UInt = Integer.reverseBytes(toInt()).toUInt()

private val SQUARE_ROOTS: List<ULong> = PRIMES.take(16).map { sqrtBD(it).frac64() }.toList()
private val CUBE_ROOTS: List<ULong> = PRIMES.map { cbrtBD(it).frac64() }.toList()

internal val MD5_CONST: List<UInt> = (1..64).map {
    sin(it.toDouble()).absoluteValue.frac32()
}
internal val SHA1_CONST: List<UInt> = intArrayOf(2, 3, 5, 10).map {
    sqrt(it / 16.0).frac32()
}
internal val SHA256_CONST: List<UInt> = CUBE_ROOTS.take(64).map { (it shr 32).toUInt() }
internal val SHA512_CONST: List<ULong> = CUBE_ROOTS

internal val SHA1_INIT: List<UInt> = uintArrayOf(
    0x01234567U, 0x89abcdefU, 0xfedcba98U, 0x76543210U, 0xf0e1d2c3U
).map(UInt::reverseBytes)
internal val MD5_INIT: List<UInt> = SHA1_INIT.take(4)
internal val SHA224_INIT: List<UInt> = SQUARE_ROOTS.drop(8).map(ULong::toUInt)
internal val SHA256_INIT: List<UInt> = SQUARE_ROOTS.take(8).map { (it shr 32).toUInt() }
internal val SHA384_INIT: List<ULong> = SQUARE_ROOTS.drop(8)
internal val SHA512_INIT: List<ULong> = SQUARE_ROOTS.take(8)
internal val SHA512T_INIT: List<ULong> = SHA512_INIT.map { it xor 0xa5a5a5a5a5a5a5a5U }
