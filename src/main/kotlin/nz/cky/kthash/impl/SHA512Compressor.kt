package nz.cky.kthash.impl

internal object SHA512Compressor : Compressor<ULongArray> {
    override fun compress(hash: ULongArray, message: ULongArray): ULongArray {
        require(hash.size == 8)
        val schedule = rollingSequence(message) {
            a + b.σ0 + j + o.σ1
        }
        return SHA512_CONST.asSequence().zip(schedule) { a, b -> a + b }
            .fold(hash) { (a, b, c, d, e, f, g, h), i ->
                val t1 = h + e.Σ1 + choose(e, f, g) + i
                ulongArrayOf(
                    t1 + a.Σ0 + majority(a, b, c),
                    a, b, c, d + t1, e, f, g
                )
            }
    }
}
