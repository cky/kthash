package nz.cky.kthash.impl

import java.util.BitSet

internal class PrimesUpTo(n: Int) : IntIterator() {
    private val bitCount: Int = (n - 1) / 2
    private val bits: BitSet = BitSet(bitCount)
    private var cur: Int = -1

    override fun hasNext(): Boolean = cur < bitCount

    override fun nextInt(): Int = when (val i = cur) {
        -1 -> {
            cur = 0
            2
        }
        in 0 until bitCount -> (i * 2 + 3).also { j ->
            (i + j + i * j until bitCount step j).forEach(bits::set)
            cur = bits.nextClearBit(i + 1)
        }
        else -> throw NoSuchElementException()
    }
}
