@file:Suppress("NonAsciiCharacters", "ClassName", "UnstableApiUsage")

package nz.cky.kthash.impl

import com.google.common.hash.HashCode
import com.google.common.hash.Hasher
import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.nio.channels.WritableByteChannel

internal open class IntMerkleDamgård(
    private val compressor: Compressor<UIntArray>,
    initialHash: List<UInt>,
    private val messageSize: Int,
    private val endianness: ByteOrder = ByteOrder.BIG_ENDIAN
) : DefaultedHasher, WritableByteChannel {
    private val hash: UIntArray = initialHash.toUIntArray()
    private val buffer: ByteBuffer = ByteBuffer.allocate(messageSize * WORD_SIZE)
        .order(endianness)
    private var count: ULong = 0U
    private var done: Boolean = false

    override fun putByte(b: Byte): Hasher = apply {
        checkPutPreconditions()
        buffer.put(b)
        compressIfNecessary()
        addToCounter(1)
    }

    override fun putBytes(b: ByteBuffer): Hasher = apply { write(b) }
    override fun putBytes(bytes: ByteArray): Hasher = putBytes(ByteBuffer.wrap(bytes))
    override fun putBytes(bytes: ByteArray, off: Int, len: Int): Hasher =
        putBytes(ByteBuffer.wrap(bytes, off, len))

    override fun hash(): HashCode {
        if (!done) {
            putPadding()
        }
        val buf = ByteBuffer.allocate(hash.size * WORD_SIZE)
            .order(endianness)
        buf.asIntBuffer().put(hash.asIntArray())
        return HashCode.fromBytes(buf.array())
    }

    private fun checkPutPreconditions() {
        assert(buffer.hasRemaining())
        check(!done) { "Can't add more data after hash() has been called" }
    }

    internal fun hashAsUIntArray(): UIntArray {
        if (!done) {
            putPadding()
        }
        return hash
    }

    private fun putPadding() {
        checkPutPreconditions()
        buffer.put(-128)
        compressIfNecessary()
        if (buffer.remaining() < COUNTER_SIZE) {
            buffer.put(ByteArray(buffer.remaining()))
            compress()
            assert(buffer.remaining() >= COUNTER_SIZE)
        }
        if (buffer.remaining() > COUNTER_SIZE) {
            buffer.put(ByteArray(buffer.remaining() - COUNTER_SIZE))
        }
        buffer.putLong(count.toLong())
        compress()
        done = true
    }

    private fun compress() {
        assert(!buffer.hasRemaining())
        val message = UIntArray(messageSize)
        buffer.flip().asIntBuffer().apply {
            get(message.asIntArray())
            buffer.position += position * WORD_SIZE
        }
        val result = compressor.compress(hash, message)
        assert(result.size == hash.size)
        result.forEachIndexed { i, value ->
            hash[i] += value
        }
        buffer.compact()
    }

    private fun compressIfNecessary() {
        if (!buffer.hasRemaining()) {
            compress()
        }
    }

    private fun addToCounter(size: Int) {
        assert(size >= 0)
        val newCount = count + size.toULong() * Byte.SIZE_BITS.toUInt()
        check(newCount >= count) { "Too much data to hash" }
        count = newCount
    }

    override fun isOpen(): Boolean = !done

    override fun close() {
        done = true
    }

    override fun write(src: ByteBuffer): Int {
        checkPutPreconditions()
        var total = 0
        while (src.remaining() > buffer.remaining()) {
            val slice = src.slice()
            val size = buffer.remaining()
            slice.limit = size
            buffer.put(slice)
            compress()
            src.position += size
            addToCounter(size)
            total += size
        }
        val size = src.remaining()
        buffer.put(src)
        compressIfNecessary()
        addToCounter(size)
        return total + size
    }

    private companion object {
        private const val WORD_SIZE: Int = UInt.SIZE_BYTES
        private const val COUNTER_SIZE: Int = ULong.SIZE_BYTES
    }
}
