package nz.cky.kthash.impl

internal object MD5Compressor : Compressor<UIntArray> {
    override fun compress(hash: UIntArray, message: UIntArray): UIntArray {
        require(hash.size == 4)
        require(message.size == 16)
        return MD5_CONST.foldIndexed(hash) { round, (a, b, c, d), i ->
            uintArrayOf(
                d,
                b + ((a + roundSpecific(round, b, c, d) + message[indexForRound(round)] + i)
                        rotateLeft rotationForRound(round)),
                b, c
            )
        }
    }

    private fun invalidRound(round: Int): Nothing =
        throw IllegalArgumentException("Invalid round: $round")

    private fun roundSpecific(round: Int, b: UInt, c: UInt, d: UInt): UInt = when (round) {
        in 0..15 -> choose(b, c, d)
        in 16..31 -> choose(d, b, c)
        in 32..47 -> parity(b, c, d)
        in 48..63 -> c xor (b or d.inv())
        else -> invalidRound(round)
    }

    private fun indexForRound(round: Int): Int = when (round) {
        in 0..15 -> round
        in 16..31 -> (5 * round + 1) % 16
        in 32..47 -> (3 * round + 5) % 16
        in 48..63 -> (7 * round) % 16
        else -> invalidRound(round)
    }

    private fun rotationForRound(round: Int): Int = (round % 4).let {
        it * 5 + 4 +
                when (round) {
                    in 0..15 -> 3
                    in 16..31 -> if (it in 1..2) 0 else 1
                    in 32..47 -> (it + 1) and -2
                    in 48..63 -> if (it in 1..2) 1 else 2
                    else -> invalidRound(round)
                }
    }
}
