/*
 * Copyright (C) 2011 The Guava Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you
 * may not use this file except in compliance with the License. You may
 * obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

@file:Suppress("UnstableApiUsage")

package nz.cky.kthash.impl

import com.google.common.base.Preconditions
import com.google.common.hash.Funnel
import com.google.common.hash.HashCode
import com.google.common.hash.HashFunction
import com.google.common.hash.Hasher
import nz.cky.kthash.hash
import java.nio.ByteBuffer
import java.nio.charset.Charset

/**
 * Default implementations of [HashFunction] methods copied over
 * from Guava's `AbstractHashFunction`, but as default methods.
 */
internal interface DefaultedHashFunction : HashFunction {
    @JvmDefault
    override fun <T : Any> hashObject(instance: T, funnel: Funnel<in T>): HashCode =
        hash { putObject(instance, funnel) }

    @JvmDefault
    override fun hashUnencodedChars(input: CharSequence): HashCode =
        hash(input.length * 2) { putUnencodedChars(input) }

    @JvmDefault
    override fun hashString(input: CharSequence, charset: Charset): HashCode =
        hash { putString(input, charset) }

    @JvmDefault
    override fun hashInt(input: Int): HashCode =
        hash(4) { putInt(input) }

    @JvmDefault
    override fun hashLong(input: Long): HashCode =
        hash(8) { putLong(input) }

    @JvmDefault
    override fun hashBytes(input: ByteArray): HashCode =
        hashBytes(input, 0, input.size)

    @JvmDefault
    override fun hashBytes(input: ByteArray, off: Int, len: Int): HashCode {
        Preconditions.checkPositionIndexes(off, off + len, input.size)
        return hash(len) { putBytes(input, off, len) }
    }

    @JvmDefault
    override fun hashBytes(input: ByteBuffer): HashCode =
        hash(input.remaining()) { putBytes(input) }

    @JvmDefault
    override fun newHasher(expectedInputSize: Int): Hasher {
        require(expectedInputSize >= 0) {
            "expectedInputSize must be >= 0 but was $expectedInputSize"
        }
        return newHasher()
    }
}
