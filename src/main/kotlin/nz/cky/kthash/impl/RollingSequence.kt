package nz.cky.kthash.impl

import java.util.LinkedList
import java.util.Queue

internal fun <T> rollingSequence(items: Collection<T>, block: DSL<T>.() -> T) =
    Sequence { RollingIterator(items, block) }

private class RollingIterator<T>(
    items: Collection<T>,
    private val block: DSL<T>.() -> T
) : Iterator<T> {
    init {
        require(items.size == 16) {
            "Incoming block needs to have 16 items: $items"
        }
    }

    private val linked: LinkedList<T> = LinkedList(items)
    private val dsl: DSL<T> = DSL(linked)

    override fun hasNext(): Boolean = true
    override fun next(): T = linked.roll(dsl.block())
}

@Suppress("unused")
internal class DSL<T>(private val list: List<T>) {
    val a: T get() = list[0]
    val b: T get() = list[1]
    val c: T get() = list[2]
    val d: T get() = list[3]
    val e: T get() = list[4]
    val f: T get() = list[5]
    val g: T get() = list[6]
    val h: T get() = list[7]
    val i: T get() = list[8]
    val j: T get() = list[9]
    val k: T get() = list[10]
    val l: T get() = list[11]
    val m: T get() = list[12]
    val n: T get() = list[13]
    val o: T get() = list[14]
    val p: T get() = list[15]
}

private fun <T> Queue<T>.roll(value: T): T {
    offer(value)
    return poll()
}
