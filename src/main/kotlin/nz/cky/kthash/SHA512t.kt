@file:Suppress("UnstableApiUsage")

package nz.cky.kthash

import com.google.common.hash.HashCode
import com.google.common.hash.Hasher
import nz.cky.kthash.impl.DefaultedHashFunction
import nz.cky.kthash.impl.LongMerkleDamgård
import nz.cky.kthash.impl.SHA512Compressor
import nz.cky.kthash.impl.SHA512T_INIT
import java.nio.ByteBuffer

class SHA512t(private val t: Int) : DefaultedHashFunction {
    init {
        require(t in 1..511) { "Invalid truncation factor: $t" }
        require(t != 384) { "Use SHA-384, not SHA-512/384" }
    }

    private val factory: () -> Hasher = when (t) {
        256 -> ::Hasher256
        224 -> ::Hasher224
        else -> ::HasherGeneric
    }

    private val initialHash: List<ULong> by lazy { makeInitialHash(t) }

    override fun newHasher(): Hasher = factory()
    override fun bits(): Int = t

    private companion object {
        private fun makeInitialHash(t: Int): List<ULong> =
            LongMerkleDamgård(SHA512Compressor, SHA512T_INIT, 16)
                .apply { putString("SHA-512/$t", Charsets.US_ASCII) }
                .hashAsULongArray().toList()
    }

    private class Hasher256 : LongMerkleDamgård(SHA512Compressor, initialHash, 16) {
        override fun hash(): HashCode {
            val buf = ByteBuffer.allocate(32)
            buf.asLongBuffer().put(hashAsULongArray().asLongArray(), 0, 4)
            return HashCode.fromBytes(buf.array())
        }

        private companion object {
            private val initialHash: List<ULong> = makeInitialHash(256)
        }
    }

    private class Hasher224 : LongMerkleDamgård(SHA512Compressor, initialHash, 16) {
        override fun hash(): HashCode {
            val buf = ByteBuffer.allocate(28)
            val origHash = hashAsULongArray()
            buf.asLongBuffer().put(origHash.asLongArray(), 0, 3)
            buf.putInt(24, (origHash[3] shr 32).toInt())
            return HashCode.fromBytes(buf.array())
        }

        private companion object {
            private val initialHash: List<ULong> = makeInitialHash(224)
        }
    }

    private inner class HasherGeneric : LongMerkleDamgård(SHA512Compressor, initialHash, 16) {
        override fun hash(): HashCode {
            val origHash = super.hash().asBytes()
            val newSize = t / 8
            val leftoverBits = t % 8
            if (leftoverBits == 0) {
                return HashCode.fromBytes(origHash.sliceArray(0 until newSize))
            }
            origHash[newSize] = (origHash[newSize].toInt()
                    and 255.shl(8 - leftoverBits)).toByte()
            return HashCode.fromBytes(origHash.sliceArray(0..newSize))
        }
    }
}
