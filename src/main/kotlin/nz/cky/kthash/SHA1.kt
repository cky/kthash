@file:Suppress("UnstableApiUsage")

package nz.cky.kthash

import com.google.common.hash.Hasher
import nz.cky.kthash.impl.DefaultedHashFunction
import nz.cky.kthash.impl.IntMerkleDamgård
import nz.cky.kthash.impl.SHA1Compressor
import nz.cky.kthash.impl.SHA1_INIT

object SHA1 : DefaultedHashFunction {
    override fun newHasher(): Hasher = SHA1Hasher()

    override fun bits(): Int = 160

    private class SHA1Hasher : IntMerkleDamgård(SHA1Compressor, SHA1_INIT, 16)
}
