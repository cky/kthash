@file:Suppress("UnstableApiUsage")

package nz.cky.kthash

import com.google.common.hash.Hasher
import nz.cky.kthash.impl.DefaultedHashFunction
import nz.cky.kthash.impl.LongMerkleDamgård
import nz.cky.kthash.impl.SHA512Compressor
import nz.cky.kthash.impl.SHA512_INIT

object SHA512 : DefaultedHashFunction {
    override fun newHasher(): Hasher = SHA512Hasher()

    override fun bits(): Int = 512

    private class SHA512Hasher : LongMerkleDamgård(SHA512Compressor, SHA512_INIT, 16)
}
