@file:Suppress("UnstableApiUsage")

package nz.cky.kthash

import com.google.common.hash.HashCode
import com.google.common.hash.Hasher
import nz.cky.kthash.impl.DefaultedHashFunction
import nz.cky.kthash.impl.LongMerkleDamgård
import nz.cky.kthash.impl.SHA384_INIT
import nz.cky.kthash.impl.SHA512Compressor
import java.nio.ByteBuffer

object SHA384 : DefaultedHashFunction {
    override fun newHasher(): Hasher = SHA384Hasher()

    override fun bits(): Int = 384

    private class SHA384Hasher : LongMerkleDamgård(SHA512Compressor, SHA384_INIT, 16) {
        override fun hash(): HashCode {
            val buf = ByteBuffer.allocate(48)
            buf.asLongBuffer().put(hashAsULongArray().asLongArray(), 0, 6)
            return HashCode.fromBytes(buf.array())
        }
    }
}
