@file:Suppress("UnstableApiUsage")

package nz.cky.kthash

import com.google.common.hash.HashCode
import com.google.common.hash.HashFunction
import com.google.common.hash.Hasher

inline fun HashFunction.hash(block: Hasher.() -> Unit): HashCode =
    newHasher().apply(block).hash()

inline fun HashFunction.hash(expectedInputSize: Int, block: Hasher.() -> Unit): HashCode =
    newHasher(expectedInputSize).apply(block).hash()
