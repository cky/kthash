@file:Suppress("UnstableApiUsage")

package nz.cky.kthash

import com.google.common.hash.Hasher
import nz.cky.kthash.impl.DefaultedHashFunction
import nz.cky.kthash.impl.IntMerkleDamgård
import nz.cky.kthash.impl.SHA256Compressor
import nz.cky.kthash.impl.SHA256_INIT

object SHA256 : DefaultedHashFunction {
    override fun newHasher(): Hasher = SHA256Hasher()

    override fun bits(): Int = 256

    private class SHA256Hasher : IntMerkleDamgård(SHA256Compressor, SHA256_INIT, 16)
}
